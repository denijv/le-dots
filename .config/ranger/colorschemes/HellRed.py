from ranger.gui.colorscheme import ColorScheme
from ranger.gui.color import default_colors, reverse, bold, normal, default

class HellRed(ColorScheme):
    progress_bar_color = 000000

    def use(self, context):
        fg, bg, attr = default_colors
        defs = 124

        if context.reset:
            return default_colors

        elif context.in_browser:
            if context.selected:
                attr = reverse

            else:
                attr = normal

            if context.image:
                fg = defs

            if context.audio:
                fg = defs

            if context.document:
                fg = defs

            if context.directory:
                attr |= bold
                fg = defs


        return fg, bg, attr
