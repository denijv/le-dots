set fish_greeting

set TERM "xterm-256color"

set -Ux EDITOR vim

set -Ux XDG_CONFIG_DIRS ~/.config

set -Ux XDG_DATA_HOME ~/.local/share

set -Ux PATH ~/.local/bin /etc/profile ~/.dotbare

set -Ux ANDROID_SDK_ROOT /home/denijv/Android/Sdk

set -Ux FZF_DEFAULT_COMMAND find .

fish_vi_key_bindings

alias v=vim
alias r=ranger
alias m=mocp
alias p=pass
alias s=sensors
alias x='sxiv -b -f'
alias f=fzf
alias g=gpg
alias n=nodemon
alias c=clear
alias z=zathura
alias u='udiskie'
alias b='bluetuith'

alias uu='udiskie-umount -a'
alias es='r /run/media/denijv/'
alias py='python /home/denijv/Coding/Python/py-mover/mover.py'

alias rns='npx react-native start'
alias rna='npx react-native run-android'

alias d=dotbare
alias da='dotbare add'
alias daf='dotbare fadd -f'
alias dad='dotbare fadd -d'
alias dc='dotbare commit'
alias dp='dotbare push'
alias ds='dotbare status'

alias pi='pass insert'
alias pe='pass edit'
alias pg='pass generate -c'
alias pc='pass -c'
alias po='pass otp -c'
alias pgp='pass git push'
alias gpp='pass git pull'

alias y=yadm
alias ys='yadm status'
alias yp='yadm push'
alias ya='yadm add'
alias yc='yadm commit'

alias tn='ten new'
alias te='ten edit'
alias tl='ten list'

alias vs='vim scp://deni@10.0.0.172:3090/'

alias gdk='gpg --delete-key'

alias server='ssh deni@10.0.0.172 -p 3090'

alias shred='sudo shred'

alias sa='sftp -oPort=3090 deni@10.0.0.172'

alias clean='yay -Rsn $(yay -Qdtq)'

function fish_prompt
	set_color "#F9C9C9"
    echo -n (basename $PWD)
	set_color "#FFFFFF"
    echo -n ' ) '
end

function fv
	set -l file_name $(fzf) | cut -c 1-

	if [ "$file_name" != "" ]
		v $file_name
	else
		return 0
	end
end

function xc
	set -l source $(xrandr | grep "HDMI" | awk '{print $1}' | fzf)

	if [ "$source" != "" ]
		xrandr --output "$source" --mode 1920x1080
	else
		return 0
	end

end

function nc
	set -l name $(nmcli con show | awk '{print $1}' | fzf)
end


function fish_user_key_bindings
end

