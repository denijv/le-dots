# The Encrypted Notes

#By Denis Vasquez

#https://gitlab.com/denisjv/le-dots/-/blob/main/.config/fish/functions/ten.fish

function ten --argument-name 'action' 'file_name'

set temporary_file $(mktemp /tmp/XXXXX-$file_name.md)

set main_directory ~/Syncthing/My/HThoughts

switch $action

case 'new'

	if test -z $file_name
		echo 'no file name'
		return 0
	end


	# Check if the file exists

	if test -e $main_directory/$file_name

		echo $file_name already exists
		return 0

	else

		$EDITOR $temporary_file

		# if file size is greater than 0 then save file

		if test -s $temporary_file
			
			g --quiet --encrypt -r 46004A0F784FEC3E9349D8A3C058C32A6461FBF8 --output $main_directory/$file_name $temporary_file

		end
	end


case 'edit'
	if test -z $file_name
		echo 'no file name'
		return 0
	end


	if test -e $main_directory/$file_name

		g --quiet --decrypt --yes --output $temporary_file $main_directory/$file_name

		$EDITOR $temporary_file

		g --quiet --encrypt --yes -r 46004A0F784FEC3E9349D8A3C058C32A6461FBF8 --output $main_directory/$file_name $temporary_file
	else
		echo $file_name doesnt exist
	end

case 'list'
	ls $main_directory

case 'delete'

	if test -z $file_name
		echo 'no file name'
		return 0
	end


	while true
		read -l -P 'Confirm Delete [y/N] ' confirm
		switch $confirm
			case Y y
				
				shred -u $main_directory/$file_name

			case N n ''
				return 0
		end
	end
end

shred -u $temporary_file

end
