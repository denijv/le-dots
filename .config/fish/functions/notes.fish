function notes --argument-names note

	if test -z $note
		echo 'No note'
		return 0
	end

	set file_dir $(readlink -f $note)


	set file_name $(readlink -f $file_dir | awk -F '.' '{print $(NF-1)}')

	set file_extension $(readlink -f $file_dir | awk -F '.' '{print $NF}')



	if test $file_extension = 'txt'
			
		g --quiet --encrypt -r 46004A0F784FEC3E9349D8A3C058C32A6461FBF8 --output $file_name.gpg $file_dir

		if test -e $file_name.gpg
			shred -u $file_dir
		end


	end

	if test $file_extension = 'gpg'
		gpg --quiet --decrypt --output $file_name.txt $file_dir


		if test -e $file_name.txt
			shred -u $file_dir

			vim $file_name.txt

			g --quiet --encrypt -r 46004A0F784FEC3E9349D8A3C058C32A6461FBF8 --output $file_name.gpg $file_name.txt

			shred -u $file_name.txt
		end
		
		

	end

end
