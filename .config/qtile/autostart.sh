#! /bin/bash
ly &
qtile &
nitrogen --restore &
picom --experimental-backends &
dunst &
flameshot &
syncthing &
