from libqtile.command import lazy
from libqtile.config import Key, Screen, Group, Match
from libqtile import hook, bar, widget
from libqtile.dgroups import simple_key_binder
from libqtile.layout.floating import Floating
from libqtile.layout.columns import Columns

import subprocess
import os

mod = "mod4"
alt = "mod1"

terminal = "alacritty"
browser = "librewolf"

username = "denijv"

screenshot_path = "/home/"+ username + "/Pictures/Screenshots"

keys = [

    Key([mod], "Return", lazy.spawn(terminal)),

    Key([mod], "w", lazy.spawn(browser)),

    Key([mod], "d", lazy.spawn("rofi -show")),

    Key([mod], "q", lazy.window.kill()),

    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),

    Key([mod], "f", lazy.window.toggle_fullscreen()),

    Key([alt], "j", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")),
    Key([alt], "k", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")),
    Key([alt], "m", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ 0%")),

    Key([alt], "u", lazy.spawn("betterlockscreen -l")),

    Key([alt], "h", lazy.spawn("flameshot full -p " + screenshot_path)),
    Key([alt], "l", lazy.spawn("flameshot gui -p " + screenshot_path)),

    Key([mod, "shift"], "r", lazy.restart()),

    Key([mod, "shift"], "f", lazy.window.toggle_floating()),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),

    Key([mod, "control"], "h", lazy.layout.grow_left()),
    Key([mod, "control"], "l", lazy.layout.grow_right()),
    Key([mod, "control"], "j", lazy.layout.grow_down()),
    Key([mod, "control"], "k", lazy.layout.grow_up()),

    Key([mod, alt], "j", lazy.spawn("pkexec /usr/bin/brillo -q -U 5")),
    Key([mod, alt], "k", lazy.spawn("pkexec /usr/bin/brillo -q -A 5")),

]

groups = [
    Group("", layout='Columns', matches=[Match(wm_class=["LibreWolf"])]), # Browser

    Group("", layout='columns', matches=[Match(wm_class=["Alacritty"])]), # Terminals

    Group("", layout='Columns', matches=[Match(wm_class=["Rawtherapee"])]), # Image Editing

    Group("辶", layout='Columns', matches=[Match(wm_class=["Olive"])]), # Video Editing

    Group("", layout='Columns', matches=[Match(wm_class=["jetbrains-studio", "DesktopEditors"])]), # Dev 

    Group("", layout='Columns', matches=[Match(wm_class=["Xonotic"])]), # Games

]

dgroups_key_binder = simple_key_binder(mod)

colors = [
    ["#000000"], # active

    ["#F99093"], # this_current_screen_border
    ["#F9C9C9"], # widgets text color
]

widgets_size = 14
screens = [
    Screen(
        top = bar.Bar([
            widget.Spacer(),

            widget.Battery(fontsize = widgets_size, foreground=colors[2]),

            widget.Spacer(),

            widget.Volume(foreground=colors[2]),

            widget.Spacer(),

            widget.AGroupBox(fontsize = 25, foreground=colors[2]),
            
            widget.Spacer(),

            widget.Wlan(format = "{quality}/70", foreground=colors[2]),

            widget.Spacer(),

            widget.Clock(format = "%I:%M %p", foreground=colors[2]),

            widget.Spacer()
        ], 30, margin = [10, 10, 15, 10])
    )
]
layouts = [
    Columns(border_focus=colors[1],border_normal=colors[0], margin=4, border_width=2, border_on_single=True),
    Floating()
]

@hook.subscribe.startup
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home])
