yay -S --nocleanmenu --answerdiff --answeredit --noremovemake nerd-fonts-fira-code moc gvim fish librewolf-bin alacritty pass \
pass-otp appimagelauncher picom scrcpy ueberzug python-pip sxiv rofi xonotic \
lightdm-slick-greeter nitrogen npm nodejs android-file-transfer ranger man-db \
betterlockscreen cava syncthing onlyoffice libnotify dunst flameshot \
android-studio udiskie ntfs-3g python-pipenv-git redis java-openjdk-bin fzf \
virtualbox linux-headers auto-cpufreq pipewire-jack pipewire-alsa pipewire-pulse qjackctl \
zathura zathura-pdf-mupdf texlive-full nextcloud-client mpd ncmpcpp bombadillo acpid qtile-extras 


curl -sSL github.com/passff/passff-host/releases/latest/download/install_host_app.sh | bash -s -- librewolf

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

mkdir ~/Syncthing ~/Music ~/Coding ~/Pictures/Screenshots ~/Videos

betterlockscreen -u ~/Pictures/Wallpapers/hellsing-lock.jpg

git config --global user.email "denijv@me"
git config --global user.name "denijv"

git clone https://gitlab.com/denijv/le-dots.git ~/.password-store

sudo systemctl enable auto-cpufreq.service

pip install iwlib

sudo killall lightdm
