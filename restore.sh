yay -S gvim fish freetube-bin librewolf alacritty
yay -S dotbare pass xclip appimagelauncher
yay -S pass-otp htop picom scrcpy ueberzug
yay -S python-pip sxiv rofi xonotic vim-plug
yay -S lightdm-slick-greeter nitrogen nodejs
yay -S android-file-transfer ranger man-db
yay -S python-pywal betterlockscreen
yay -S cava syncthing npm xautolock
yay -S pfetch colorpicker onlyoffice libnotify dunst
yay -S flameshot canto-curses android-studio ntfs-3g
yay -S moon-buggy python-pipenv-git redis java-open-jdk-bin
yay -S kdeconnect watchman fzf linux-headers virtualbox udiskie
yay -S xf86-video-intel 

#obsidian editor, 

curl -sSL github.com/passff/passff-host/releases/latest/download/install_host_app.sh | bash -s -- librewolf

#pulseaudio-ctl m

mkdir ~/Syncthing ~/Music ~/Coding ~/Coding/Python ~/Pictures/Screenshots

betterlockscreen -u ~/Pictures/Wallpapers/hellsing-lock.jpg

git config --global user.email "denijv@me"
git config --global user.name "denijv"

d git push --set-upstream origin main

git clone https://gitlab.com/denijv/le-dots.git ~/.password-store

sudo killall lightdm
