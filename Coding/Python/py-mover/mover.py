#https://www.thepythoncode.com/article/organize-files-by-extension-with-python

import os
import glob
import shutil

# dictionary mapping each extension with its corresponding folder
# For example, 'jpg', 'png', 'ico', 'gif', 'svg' files will be moved to 'images' folder
# feel free to change based on your needs
extensions = {
    "jpg": "/home/denijv/Pictures",
    "png": "/home/denijv/Pictures",
    "svg": "/home/denijv/Pictures",
    "pdf": "/home/denijv/Documents/PDF",
    "py": "/home/denijv/Coding/Python",
    "mp3": "/home/denijv/Music",
    "wav": "/home/denijv/Music",
    "mp4": "/home/denijv/Videos",
}

path = "/home/denijv/Downloads"
# setting verbose to 1 (or True) will show all file moves
# setting verbose to 0 (or False) will show basic necessary info
verbose = 0
for extension, folder_name in extensions.items():
    # get all the files matching the extension
    files = glob.glob(os.path.join(path, f"*.{extension}"))
    print(f"[*] Found {len(files)} files with {extension} extension")
    if not os.path.isdir(os.path.join(path, folder_name)) and files:
        # create the folder if it does not exist before
        print(f"[+] Making {folder_name} folder")
        os.mkdir(os.path.join(path, folder_name))
    for file in files:
        # for each file in that extension, move it to the correponding folder
        basename = os.path.basename(file)
        dst = os.path.join(path, folder_name, basename)
        if verbose:
            print(f"[*] Moving {file} to {dst}")
        shutil.move(file, dst)
