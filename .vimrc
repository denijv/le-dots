set number relativenumber
set laststatus=2
set wrap
set encoding=utf-8
set noswapfile
set scrolloff=8
set tabstop=4
set autochdir
set backspace=indent,eol,start
set nocp
set showcmd
"set spell spelllang=en_us

filetype plugin on

syntax enable

if filereadable(expand("~/.vimrc.plug"))
	source ~/.vimrc.plug
endif

colorscheme wal

let mapleader = " "

let &t_EI .= "\e[5 q"
let &t_SI .= "\e[5 q"
let &t_SR .= "\e[5 q"

"let g:airline_theme = 'deus'
let g:airline_section_x = 'xx'
let g:airline_section_y = 'yy'
let g:airline_section_z = 'zz'
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline#extensions#hunks#non_zero_only = 1
let g:airline#extensions#wordcount#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled = 1

let g:gitgutter_enabled = 1
let g:gitgutter_map_keys = 0

let g:vimtex_view_method='zathura'

let g:tmpl_search_paths = ['~/vimplates']

highlight GitGutterAdd guifg=#009900 ctermfg=Green
highlight GitGutterChange guifg=#ffffff ctermfg=White
highlight GitGutterDelete guifg=#ff2222 ctermfg=Red

nnoremap gs :Git status<CR>
nnoremap gm :0G<CR>
nnoremap ga :Git add .<CR>
nnoremap gc :Git commit<CR>
nnoremap gp :Git push<CR>
nnoremap <leader>q :bp<CR>
nnoremap <leader>w :bn<CR>
nnoremap <leader>e :bd<CR>
nnoremap <leader>o :FZF<CR>
nnoremap mp :MarkdownPreview<CR>
nnoremap <leader>s :w<CR>
nnoremap q gqq<CR>
nnoremap <F1> <Esc>:w<CR>:!pdflatex % [filename.tex]; <CR>
nnoremap <F5> <Esc>:w<CR>:!python % [filename.py]; <CR>

nnoremap e :NERDTreeToggle<CR>
nnoremap <leader>n :NERDTreeFocus<CR>

nmap <leader>a <Plug>(coc-codeaction-selected)

vnoremap <C-c> "+y

inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

